// ==UserScript==
// @name         WhatsApp Contact Button Changer
// @namespace    https://example.com
// @version      1.0
// @description  Changes WhatsApp contact buttons on Facebook pages to text message buttons
// @author       Your Name
// @match        https://www.facebook.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Function to open the default text messaging app
    function openTextMessagingApp(phoneNumber) {
        const isiOS = /iPhone|iPad|iPod/.test(navigator.userAgent);
        const isAndroid = /Android/.test(navigator.userAgent);

        if (isiOS) {
            window.location.href = `sms:&body=${encodeURIComponent(phoneNumber)}`;
        } else if (isAndroid) {
            window.location.href = `sms:${phoneNumber}`;
        } else {
            alert('Please open the text messaging app and manually enter the phone number.');
        }
    }

    // Function to replace WhatsApp contact buttons
    function replaceWhatsAppButtons() {
        const buttons = document.querySelectorAll('a[href*="api.whatsapp.com/send"]');
        buttons.forEach((button) => {
            const href = button.getAttribute('href');
            const decodedURL = decodeURIComponent(href);
            const urlParams = new URLSearchParams(decodedURL.substring(decodedURL.indexOf('?')));
            const phoneNumber = urlParams.get('phone');
            button.setAttribute('href', `sms://+${phoneNumber}`);
            button.addEventListener('click', (event) => {
                event.preventDefault();
                openTextMessagingApp(phoneNumber);
            });
            button.innerHTML = 'Text Message';
        });
    }

    // Call the function on page load
    replaceWhatsAppButtons();

    // Observe DOM changes for dynamically loaded content
    const observer = new MutationObserver(function(mutationsList, observer) {
        for (let mutation of mutationsList) {
            if (mutation.type === 'childList') {
                replaceWhatsAppButtons();
            }
        }
    });

    observer.observe(document.body, { childList: true, subtree: true });
})();
